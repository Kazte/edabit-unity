﻿using System;
using System.Linq;

public class PasswordCheck
{
    public bool Validate(string password)
    {
        var specialCharsSupported = "!@#$%^&*()+=_-{}[]:;\"'?<>,.";

        if (password.Length < 6)
            return false;

        if (password.Length > 24)
            return false;

        if (!password.Any(char.IsUpper))
            return false;

        if (!password.Any(char.IsLower))
            return false;

        if (!password.Any(char.IsDigit))
            return false;

        if (password.GroupBy(x => x).Any(g => g.Count() > 2))
            return false;

        var flag = password.Select(c =>
            !char.IsDigit(c) && !char.IsLetter(c) && !specialCharsSupported.Contains(c));

        if (flag.Count() < 0)
            return false;

        return true;
    }
}