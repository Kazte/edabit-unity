﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PasswordUI : MonoBehaviour
{
    [SerializeField]
    private Button checkBtn;

    [SerializeField]
    private InputField passwordInputField;

    private PasswordCheck passwordCheck;

    private void Awake()
    {
        passwordCheck = new PasswordCheck();

        checkBtn.onClick.AddListener(CheckPassword);
    }

    private void CheckPassword()
    {
        if (passwordCheck.Validate(passwordInputField.text))
        {
            Debug.Log("Validate");
        }
        else
        {
            Debug.Log("Error");
        }
    }
}