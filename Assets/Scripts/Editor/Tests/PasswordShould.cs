﻿using System.Linq;
using NUnit.Framework;

namespace Editor.Tests
{
    public class PasswordShould
    {
        /*
         * Create a function that validates a password to conform to the following rules:

            Length between 6 and 24 characters.
            At least one uppercase letter (A-Z).
            At least one lowercase letter (a-z).
            At least one digit (0-9).
            Maximum of 2 repeated characters.
            "aa" is OK 👍
            "aaa" is NOT OK 👎
            Supported special characters:
            ! @ # $ % ^ & * ( ) + = _ - { } [ ] : ; " ' ? < > , .
         */

        private PasswordCheck passwordCheck;

        [SetUp]
        public void Setup()
        {
            passwordCheck = new PasswordCheck();
        }

        [Test]
        public void Not_Valid_Password_When_Length_Lesser_Than_6()
        {
            var password = "Abc_3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_When_Length_Greater_Than_24()
        {
            var password = "Abcdefghijlkmnopqrtuvwxyz12456789_3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_Not_Contains_Uppercase()
        {
            var password = "abcdef_3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_Not_Contains_Lowercase()
        {
            var password = "ABCDEF_3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_Not_Contains_One_Digit()
        {
            var password = "Abcdef_r";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_Contains_3_Or_More_Chars_Repeated()
        {
            var password = "Abbbcdef_3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }

        [Test]
        public void Not_Valid_Password_Contains_Unsupported_Special_Chars()
        {
            var password = "Abbbcdef£3";

            Assert.IsFalse(passwordCheck.Validate(password));
        }
    }
}