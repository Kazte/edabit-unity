﻿using System;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace Editor.Tests
{
    public class RobotShould
    {
        private Robot robot;

        [SetUp]
        public void Setup()
        {
            robot = new Robot();
        }

        [Test]
        public void Move_Forward()
        {
            robot.ExecuteCommand(".");

            Assert.AreEqual(Vector2.right, robot.Position);
        }

        [Test]
        public void Turn_Clockwise()
        {
            robot.ExecuteCommand(">");

            Assert.AreEqual(Direction.South, robot.Direction);
        }

        [Test]
        public void Turn_AntiClockwise()
        {
            robot.ExecuteCommand("<");

            Assert.AreEqual(Direction.North, robot.Direction);
        }

        [Test]
        public void Turn_Clockwise_Then_Move_Forward()
        {
            robot.ExecuteCommand(">.");

            Assert.AreEqual(Vector2.down, robot.Position);
        }

        [Test]
        public void Turn_AntiClockwise_Then_Move_Forward()
        {
            robot.ExecuteCommand("<.");

            Assert.AreEqual(Vector2.up, robot.Position);
        }

        [Test]
        public void Execute_Custom_Command()
        {
            robot.ExecuteCommand("..<.<.>.."); // 1, 3

            Assert.AreEqual(new Vector2(1, 3), robot.Position);
        }
    }
}