﻿using System;
using System.Collections;
using UnityEngine;

public class Robot : MonoBehaviour
{
    public Vector2 Position { get; private set; }

    public Direction Direction { get; private set; }

    [SerializeField]
    private float commandTimer = 1f;

    private float commandTimerLeft = 0f;

    public Robot()
    {
        Position = Vector2.zero;
        Direction = Direction.East;
    }

    private void Start()
    {
        StartCoroutine(ExecuteCommandCoroutine("..<.<.>.."));
    }

    private void Update()
    {
        transform.position = Position;

        switch (Direction)
        {
            case Direction.North:
                transform.localEulerAngles = new Vector3(0f, 0f, 90f);
                break;
            case Direction.East:
                transform.localEulerAngles = new Vector3(0f, 0f, 0f);
                break;
            case Direction.South:
                transform.localEulerAngles = new Vector3(0f, 0f, 270f);
                break;
            case Direction.West:
                transform.localEulerAngles = new Vector3(0f, 0f, 180f);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public IEnumerator ExecuteCommandCoroutine(string commandsString)
    {
        foreach (var command in commandsString)
        {
            ExecuteCommandInternal(command);
            yield return new WaitForSeconds(1f);
        }
    }

    public void ExecuteCommand(string commandsString)
    {
        foreach (var command in commandsString)
        {
            ExecuteCommandInternal(command);
        }
    }

    private void ExecuteCommandInternal(char command)
    {
        if (command == '.')
            switch (Direction)
            {
                case Direction.North:
                    Position += Vector2.up;
                    break;
                case Direction.East:
                    Position += Vector2.right;
                    break;
                case Direction.South:
                    Position += Vector2.down;
                    break;
                case Direction.West:
                    Position += Vector2.left;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        else if (command == '>')
            switch (Direction)
            {
                case Direction.North:
                    Direction = Direction.East;
                    break;
                case Direction.East:
                    Direction = Direction.South;
                    break;
                case Direction.South:
                    Direction = Direction.West;
                    break;
                case Direction.West:
                    Direction = Direction.North;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        else if (command == '<')
            switch (Direction)
            {
                case Direction.North:
                    Direction = Direction.West;
                    break;
                case Direction.East:
                    Direction = Direction.North;
                    break;
                case Direction.South:
                    Direction = Direction.East;
                    break;
                case Direction.West:
                    Direction = Direction.South;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
    }
}


public enum Direction
{
    North,
    East,
    South,
    West
}